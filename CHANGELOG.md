# Change Log
All notable changes to this project will be documented in this file.

## [0.0.0.1] - 10-01-2019
- Encryption Settings Added.
- Text and File Encryption Added.
- AES and RSA Support Added.
