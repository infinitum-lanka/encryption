﻿using System.IO;

namespace Infinitum.Security.Encryption {
    public interface IEncryption {
        #region Text
        string Encrypt(string plainText);
        string Decrypt(string cipherText);
        #endregion

        #region File
        byte[] FileToByteArray(string fileName);

        void WriteToFile(string outputFile, MemoryStream memoryStream);

        MemoryStream EncryptFile(byte[] plainTextBytes);
        MemoryStream DecryptFile(byte[] cipherTextBytes);
        #endregion
    }
}
