﻿using System;
using System.Text;

namespace Infinitum.Security.Encryption {
    public enum SettingValidation {
        NoFaults = 1,
        Expired = 2,
        LimitReached = 3
    }

    public class SettingData {
        public string MaxViewLimit { get; set; } /* 0000 */
        public string DateExpire { get; set; } /* DD/MM/YYYY */
        public string ViewCount { get; set; } /* 0000 */

        public SettingData() {
            MaxViewLimit = "0000";
            DateExpire = "UU/UU/UUUU";
            ViewCount = "0000";
        }
    }

    public class Settings {
        public const int settingsLength = 68;
        public const string seperator = "#IR#";

        private const StringSplitOptions stringSplitOptions = StringSplitOptions.RemoveEmptyEntries;

        public SettingData data = new SettingData();

        private string fileName = string.Empty;

        public Settings() { }
        public Settings (string fileName) {
            this.fileName = fileName;
        }

        #region Validation
        private SettingValidation ValidateViewLimit() {
            try {
                short _maxLimit = short.Parse(data.MaxViewLimit);
                short _views = short.Parse(data.ViewCount);

                if (_views > _maxLimit) return SettingValidation.LimitReached;
                _views += 1;

                string _viewLimit = $"{_views}";

                if (_views < 10) _viewLimit = $"000{_views}";
                else if (_views >= 10 && _views < 100) _viewLimit = $"00{_views}";
                else if (_views >= 100 && _views < 1000) _viewLimit = $"0{_views}";

                data.ViewCount = _viewLimit;

                return SettingValidation.NoFaults;
            } catch (Exception ex) {
                throw ex;
            }
        }
        private SettingValidation ValidateViewDate() {
            try {
                string[] _dateCollection = data.DateExpire.Split(new string[] { "/" }, stringSplitOptions);

                short _day = short.Parse(_dateCollection[0]),
                      _month = short.Parse(_dateCollection[1]),
                      _year = short.Parse(_dateCollection[2]);

                DateTime _expireDate = new DateTime(_year, _month, _day),
                         _now = DateTime.Today;

                int _compareResults = DateTime.Compare(_expireDate, _now);

                if (_compareResults != 1) return SettingValidation.Expired;

                return SettingValidation.NoFaults;
            } catch (DataMisalignedException ex) {
                throw ex;
            } catch (Exception ex) {
                throw ex;
            }
        }
        public SettingValidation Validate() {
            try {
                SettingValidation _viewDateExpired = ValidateViewDate();
                SettingValidation _viewLimitExpired = ValidateViewLimit();

                if (_viewDateExpired == SettingValidation.Expired) return SettingValidation.Expired;
                if (_viewLimitExpired == SettingValidation.LimitReached) return SettingValidation.LimitReached;

                return SettingValidation.NoFaults;
            } catch (Exception ex) {
                throw ex;
            }
        }
        #endregion

        private string CompileSettings() {
            try {
                StringBuilder _sb = new StringBuilder();

                _sb.Append($"MaxViewLimit:'{data.MaxViewLimit}'")
                .Append(",")
                .Append($"DateExpire:'{data.DateExpire}'")
                .Append(",")
                .Append($"ViewCount:'{data.ViewCount}'");

                return $"{seperator}{_sb.ToString()}{seperator}";
            } catch (Exception ex) {
                throw ex;
            }
        }
        private void DecompileSettings(byte[] jsonSettings) {
            try {
                string _jsonSettings = Encoding.UTF8.GetString(jsonSettings);

                _jsonSettings = _jsonSettings
                .Replace(seperator, string.Empty)
                .Replace("\r\n", string.Empty)
                .Replace("{", string.Empty)
                .Replace("}", string.Empty)
                .Replace("'", string.Empty);

                string[] _setCollection = _jsonSettings.Split(new string[] { "," }, stringSplitOptions);

                data.MaxViewLimit = _setCollection[0].Split(new string[] { ":" }, stringSplitOptions)[1];
                data.DateExpire = _setCollection[1].Split(new string[] { ":" }, stringSplitOptions)[1];
                data.ViewCount = _setCollection[2].Split(new string[] { ":" }, stringSplitOptions)[1];
            } catch (IndexOutOfRangeException ex) {
                throw ex;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public byte[] Write(SettingData settingsJSON, byte[] plainTextBytes) {
            try {
                this.data = settingsJSON;

                byte[] _settings = Encoding.UTF8.GetBytes(CompileSettings());

                int _dataWithSettingsLength = plainTextBytes.Length + _settings.Length;

                byte[] _dataWithSettings = new byte[_dataWithSettingsLength];

                for (int i = 0;i < settingsLength;i++) {
                    _dataWithSettings[i] = _settings[i];
                }

                for (int i = settingsLength;i < _dataWithSettingsLength;i++) {
                    _dataWithSettings[i] = plainTextBytes[i - settingsLength];
                }

                return _dataWithSettings;
            } catch (IndexOutOfRangeException ex) {
                throw ex;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public byte[] Read(byte[] cipherTextBytes) {
            int _cipherTextLength = cipherTextBytes.Length;

            byte[] _dataWithSettings = new byte[_cipherTextLength];
            byte[] _settings = new byte[settingsLength];

            bool _settingsRead = false;

            for (int i = 0;i < _cipherTextLength;i++) {
                if (i <= settingsLength - 1) {
                    _settings[i] = cipherTextBytes[i];
                } else {
                    if (!_settingsRead && i >= settingsLength) {
                        DecompileSettings(_settings);

                        SettingValidation _validation = Validate();
                        switch (_validation) {
                            case SettingValidation.Expired:
                                throw new Exception("File Expiry Date Reached");
                            case SettingValidation.LimitReached:
                                throw new Exception("File View Limit Exceeded");
                        }

                        _settings = Encoding.UTF8.GetBytes(CompileSettings());
                        _settingsRead = true;

                        _settings.CopyTo(_dataWithSettings, 0);
                    }

                    _dataWithSettings[i - settingsLength] = cipherTextBytes[i];
                }
            }

            return _dataWithSettings;
        }
    }
}