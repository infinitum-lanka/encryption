﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Infinitum.Security.Encryption {
    public sealed class Asymmetric:Core, IEncryption {
        public Asymmetric(string passPhrase, SymmetricAlgorithm symmetricAlgorithm, SettingData settingData, int passwordIterations = 2, string hashAlgorithm = "SHA1") {
            this.passPhrase = passPhrase;
            this.passwordIterations = passwordIterations;
            this.hashAlgorithm = hashAlgorithm;
            this.settingData = settingData;
            this.symmetricAlgorithm = symmetricAlgorithm;
        }

        public Asymmetric(string passPhrase, SymmetricAlgorithm symmetricAlgorithm, int passwordIterations = 2, string hashAlgorithm = "SHA1") {
            this.passPhrase = passPhrase;
            this.passwordIterations = passwordIterations;
            this.hashAlgorithm = hashAlgorithm;
            this.symmetricAlgorithm = symmetricAlgorithm;
        }

        #region Text
        public string Encrypt(string plainText) {
            try {
                byte[] _initVectorBytes = Encoding.UTF8.GetBytes(initVector);
                byte[] _saltValueBytes = Encoding.UTF8.GetBytes(saltValue);

                byte[] _plainTextBytes = Encoding.UTF8.GetBytes(plainText);

                PasswordDeriveBytes _password = new PasswordDeriveBytes(passPhrase, _saltValueBytes, hashAlgorithm, passwordIterations);

                byte[] _keyBytes = _password.GetBytes(keySize / 8);

                symmetricAlgorithm.Mode = cipherMode;

                ICryptoTransform _encryptor = symmetricAlgorithm.CreateEncryptor(_keyBytes, _initVectorBytes);

                MemoryStream _memoryStream = new MemoryStream();

                CryptoStream _cryptoStream = new CryptoStream(_memoryStream, _encryptor, CryptoStreamMode.Write);

                _cryptoStream.Write(_plainTextBytes, 0, _plainTextBytes.Length);
                _cryptoStream.FlushFinalBlock();

                byte[] _cipherTextBytes = _memoryStream.ToArray();

                _memoryStream.Close();
                _cryptoStream.Close();

                return Convert.ToBase64String(_cipherTextBytes);
            } catch (OutOfMemoryException ex) {
                throw ex;
            } catch (CryptographicUnexpectedOperationException ex) {
                throw ex;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public string Decrypt(string cipherText) {
            try {
                byte[] _initVectorBytes = Encoding.UTF8.GetBytes(initVector);
                byte[] _saltValueBytes = Encoding.UTF8.GetBytes(saltValue);

                byte[] _cipherTextBytes = Convert.FromBase64String(cipherText.Replace(" ", "+"));

                PasswordDeriveBytes _passPhrase = new PasswordDeriveBytes(passPhrase, _saltValueBytes, hashAlgorithm, passwordIterations);

                byte[] _keyBytes = _passPhrase.GetBytes(keySize / 8);

                symmetricAlgorithm.Mode = cipherMode;

                ICryptoTransform _decryptor = symmetricAlgorithm.CreateDecryptor(_keyBytes, _initVectorBytes);

                MemoryStream _memoryStream = new MemoryStream(_cipherTextBytes);

                CryptoStream _cryptoStream = new CryptoStream(_memoryStream, _decryptor, CryptoStreamMode.Read);

                byte[] _plainTextBytes = new byte[_cipherTextBytes.Length];

                int _decryptedByteCount = _cryptoStream.Read(_plainTextBytes, 0, _plainTextBytes.Length);

                _memoryStream.Close();
                _cryptoStream.Close();

                return Encoding.UTF8.GetString(_plainTextBytes, 0, _decryptedByteCount);
            } catch (OutOfMemoryException ex) {
                throw ex;
            } catch (CryptographicUnexpectedOperationException ex) {
                throw ex;
            } catch (Exception ex) {
                throw ex;
            }
        }
        #endregion

        #region File
        public MemoryStream EncryptFile(byte[] plainTextBytes) {
            try {
                /* Handle Settings - Start */
                byte[] _dataWithSettings = settings.Write(settingData, plainTextBytes);
                /* Handle Settings - End */

                byte[] _initVectorBytes = Encoding.UTF8.GetBytes(initVector);
                byte[] _saltValueBytes = Encoding.UTF8.GetBytes(saltValue);

                PasswordDeriveBytes _password = new PasswordDeriveBytes(passPhrase, _saltValueBytes, hashAlgorithm, passwordIterations);

                byte[] _keyBytes = _password.GetBytes(keySize / 8);
                randomKeyText = GetString(_keyBytes);

                symmetricAlgorithm.Mode = cipherMode;

                ICryptoTransform _encryptor = symmetricAlgorithm.CreateEncryptor(_keyBytes, _initVectorBytes);

                MemoryStream _memoryStream = new MemoryStream();

                CryptoStream _cryptoStream = new CryptoStream(_memoryStream, _encryptor, CryptoStreamMode.Write);

                _cryptoStream.Write(_dataWithSettings, 0, _dataWithSettings.Length);
                _cryptoStream.FlushFinalBlock();

                _memoryStream.Seek(0, SeekOrigin.Begin);

                return _memoryStream;
            } catch (OutOfMemoryException ex) {
                throw ex;
            } catch (CryptographicUnexpectedOperationException ex) {
                throw ex;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public MemoryStream DecryptFile(byte[] cipherTextBytes) {
            try {
                string _randomKeyText = GetRandomKeyText(passPhrase, saltValue);

                byte[] _initVectorBytes = Encoding.UTF8.GetBytes(initVector);

                symmetricAlgorithm.Mode = cipherMode;

                byte[] _keyBytes = GetBytes(_randomKeyText);
                ICryptoTransform _decryptor = symmetricAlgorithm.CreateDecryptor(_keyBytes, _initVectorBytes);

                MemoryStream _memoryStream = new MemoryStream(cipherTextBytes);

                CryptoStream _cryptoStream = new CryptoStream(_memoryStream, _decryptor, CryptoStreamMode.Read);

                byte[] _plainTextBytes = new byte[cipherTextBytes.Length];

                int _decryptedByteCount = _cryptoStream.Read(_plainTextBytes, 0, _plainTextBytes.Length);

                /* Settings - Start */
                byte[] _dataWithoutSettings = settings.Read(_plainTextBytes);
                settingData = settings.data;

                WriteToFile(EncryptedFile, EncryptFile(_dataWithoutSettings));
                /* Settings - End */

                return new MemoryStream(_dataWithoutSettings);
            } catch (OutOfMemoryException ex) {
                throw ex;
            } catch (CryptographicUnexpectedOperationException ex) {
                throw ex;
            } catch (Exception ex) {
                throw ex;
            }
        }
        #endregion
    }
}