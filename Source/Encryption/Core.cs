﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Infinitum.Security.Encryption {
    public class Core {
        protected const string initVector = "D1dfc@D4e5F#g7H8";     /* Change for Live Environments */
        protected const int keySize = 256;
        protected const CipherMode cipherMode = CipherMode.CBC;

        public string saltValue = "W#]3+oefVyogPb^Z7p+29&S&0[w#fl"; /* Change for Live Environments */

        public string hashAlgorithm = "SHA1"; /* SHA1, MD5 */
        public int passwordIterations = 2;

        protected string passPhrase = string.Empty;
        protected string randomKeyText = string.Empty;

        public SymmetricAlgorithm symmetricAlgorithm;               /* RijndaelManaged, AesManaged */

        public Settings settings = new Settings();
        public SettingData settingData = new SettingData();

        private string encryptedFile = string.Empty;
        public string EncryptedFile {
            get { return encryptedFile; }
            set {
                encryptedFile = value;
            }
        }

        public Core() {
            if (!BitConverter.IsLittleEndian) throw new Exception("Actually it's made just for Little Endian -> Little Endian so far");
        }

        public string GetString(byte[] bytes) {
            try {
                int _byteLength = bytes.Length;

                char[] _chars = new char[_byteLength / sizeof(char)];
                System.Buffer.BlockCopy(bytes, 0, _chars, 0, _byteLength);
                return new string(_chars);
            } catch (Exception ex) {
                throw ex;
            }            
        }

        public byte[] GetBytes(string randomKeyText) {
            try {
                byte[] _bytes = new byte[randomKeyText.Length * sizeof(char)];
                System.Buffer.BlockCopy(randomKeyText.ToCharArray(), 0, _bytes, 0, _bytes.Length);
                return _bytes;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public string GetRandomKeyText(string passsword, string salt) {
            try {
                byte[] _saltValueBytes = Encoding.ASCII.GetBytes(salt);

                PasswordDeriveBytes _password = new PasswordDeriveBytes(passsword, _saltValueBytes, hashAlgorithm, passwordIterations);
                byte[] _keyBytes = _password.GetBytes(keySize / 8);

                return GetString(_keyBytes);
            } catch (Exception ex) {
                throw ex;
            }
        }

        #region File
        public byte[] FileToByteArray(string fileName) {
            try {
                byte[] _plainTextBytes;

                using (MemoryStream memoryStream = new MemoryStream())
                using (FileStream _fs = new FileStream(fileName, FileMode.Open)) {
                    _fs.CopyTo(memoryStream);
                    _fs.Flush();

                    _plainTextBytes = memoryStream.ToArray();
                    memoryStream.Close();
                }

                return _plainTextBytes;
            } catch (FileNotFoundException ex) {
                throw new FileNotFoundException("File Not Found", fileName);
            } catch (Exception ex) {
                throw new Exception($"Unspecified Error Occurred: {ex.Message}");
            }
        }

        public void WriteToFile(string outputFile, MemoryStream memoryStream) {
            try {
                using (FileStream _fs = new FileStream(outputFile, FileMode.OpenOrCreate)) {
                    memoryStream.CopyTo(_fs);
                    _fs.Flush();
                }
                memoryStream.Close();
            } catch (Exception ex) {
                throw new Exception($"Unspecified Error Occurred: {ex.Message}");
            }
        }
        #endregion
    }
}
