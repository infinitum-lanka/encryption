# Infinitum Encryption

A cryptography library written in C# to protect files and text with customized encryption settings.

## Usage
### File Encryption	
```c#
string password = "strong_password_here";
string fileName = "file_name_here";
string outputFile = "file_name_here";	

SymmetricAlgorithm algorithm = new RijndaelManaged();

SettingData settings = new SettingData() {
MaxViewLimit = "0100", DateExpire = "03/01/2020"
};
/* 
	MaxViewLimit: Number of times the file can be viewed.
	DateExpire: The date on which the file become inaccessible.
*/

Asymmetric rsa = new Asymmetric(password, algorithm, settings);
byte[] fileData = rsa.FileToByteArray(fileName);
MemoryStream encryptData = rsa.EncryptFile(fileData);
rsa.WriteToFile(outputFile, encryptData);
```
### File Decryption
```c#
string password = "password_here";
string fileName = "file_name_here";
string outputFile = "file_name_here";

SymmetricAlgorithm algorithm = new RijndaelManaged();
Asymmetric rsa = new Asymmetric(password, algorithm, new SettingData());

rsa.EncryptedFile = fileName;

byte[] fileData = rsa.FileToByteArray(fileName);
MemoryStream decryptData = rsa.DecryptFile(fileData);
rsa.WriteToFile(outputFile, decryptData);
```
## Text Encryption / Decryption
```c#
SymmetricAlgorithm algorithm = new RijndaelManaged();
Asymmetric rsa = new Asymmetric(password, algorithm);

string encryptData = rsa.Encrypt("text_here");
string decryptData = rsa.Decrypt(encryptData);
```

Copyright (c) 2019 [INFINITUM](https://www.infinitum.lk). All rights reserved.